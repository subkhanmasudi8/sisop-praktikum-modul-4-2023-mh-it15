    #define HAVE_SETXATTR
    #define FUSE_USE_VERSION 28
    #define _XOPEN_SOURCE 500
    #include <fuse.h>
    #include <string.h>
    #include <time.h>
    #include <unistd.h>
    #include <stdio.h>
    #include <sys/time.h>
    #include <sys/xattr.h>
    #include <fcntl.h>
    #include <dirent.h>
    #include <errno.h>

    static const char *myPath = "/home/dimas/sisopmodul4";
    static const char *myLogFile = "/home/dimas/sisopmodul4/fs_module.log";

    void custom_logger(const char *file_path, const char *action, const char *log_type) {
        char current_time[30];
        char report[9999];
        struct tm *time_info;
        time_t now;

        time(&now);
        time_info = localtime(&now);

        strftime(current_time, sizeof(current_time), "%y%m%d-%H:%M:%S", time_info);
        sprintf(report, "%s::%s::%s::%s\n", log_type, current_time, action, file_path);
        FILE *log_file = fopen(myLogFile, "a");

        fprintf(log_file, "%s", report);
        printf("> Logged\n");

        fclose(log_file);
    }

    static int custom_getattr(const char *file_path, struct stat *stat_buffer) {
        int result;
        char full_path[9999];
        sprintf(full_path, "%s%s", myPath, file_path);
        result = lstat(full_path, stat_buffer);
        if (result == -1) return -errno;
        return 0;
    }

    static int custom_read(const char *file_path, char *buffer, size_t size, off_t offset, struct fuse_file_info *file_info) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int result = 0, file_descriptor = 0;
        (void) file_info;

        file_descriptor = open(full_path, O_RDONLY);
        if (file_descriptor == -1) return -errno;

        result = pread(file_descriptor, buffer, size, offset);
        if (result == -1) return -errno;

        close(file_descriptor);
        return result;
    }

    static int custom_readdir(const char *file_path, void *buffer, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *file_info) {
        char full_path[9999];
        char new_name[256];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        DIR *directory;
        struct dirent *dir_entry;
        (void) offset;
        (void) file_info;

        int result = 0;

        directory = opendir(full_path);
        if (directory == NULL) return -errno;

        while ((dir_entry = readdir(directory)) != NULL) {
            struct stat file_stat;
            memset(&file_stat, 0, sizeof(file_stat));

            file_stat.st_ino = dir_entry->d_ino;
            file_stat.st_mode = dir_entry->d_type << 12;

            strcpy(new_name, dir_entry->d_name);
            if (!(strcmp(new_name, ".") == 0 || strcmp(new_name, "..") == 0) && strstr(full_path, "module_")) {
                printf("%s berada pada modular dir\n", new_name);
            } else printf("%s tidak berada pada modular dir\n", new_name);

            result = (filler(buffer, new_name, &file_stat, 0));

            if (result != 0) break;
        }

        closedir(directory);
        return 0;
    }

    static int custom_open(const char *file_path, struct fuse_file_info *file_info) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int result = open(full_path, file_info->flags);
        if (result == -1) return -errno;

        file_info->fh = result;
        return 0;
    }

    static int custom_write(const char *file_path, const char *buffer, size_t size, off_t offset, struct fuse_file_info *file_info) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int file_handle = open(full_path, O_WRONLY);
        if (file_handle == -1) return -errno;
        int result = pwrite(file_handle, buffer, size, offset);
        if (result == -1) return -errno;
        close(file_handle);
        return result;
    }

    static int custom_mkdir(const char *file_path, mode_t mode) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int result = mkdir(full_path, mode);
        if (result == -1) return -errno;

        custom_logger(full_path, "MKDIR", "REPORT");
        return 0;
    }

    static int custom_access(const char *file_path, int mask) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int result = access(full_path, mask);
        if (result == -1) return -errno;
        return 0;
    }

    static int custom_create(const char *file_path, mode_t mode, struct fuse_file_info *file_info) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        (void) file_info;

        int result = creat(full_path, mode);
        if (result == -1) return -errno;

        close(result);
        custom_logger(full_path, "CREATE", "REPORT");
        return 0;
    }

    static int custom_rename(const char *from, const char *to) {
        int result = 0;
        char report[9999];

        result = rename(from, to);
        if (result == -1) return -errno;

        sprintf(report, "%s::%s", from, to);
        custom_logger(report, "RENAME", "REPORT");
        return 0;
    }

    static int custom_unlink(const char *file_path) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int result = unlink(full_path);
        if (result == -1) return -errno;

        custom_logger(full_path, "UNLINK", "FLAG");
        return 0;
    }

    static int custom_rmdir(const char *file_path) {
        char full_path[9999];
        int result;

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        if ((result = rmdir(full_path)) == -1) return -errno;

        custom_logger(full_path, "RMDIR", "FLAG");
        return 0;
    }

    static int custom_flush(const char *file_path, struct fuse_file_info *file_info) {
        // Implementasi fungsi custom_flush
        return 0;
    }

    static int custom_release(const char *file_path, struct fuse_file_info *file_info) {
        // Implementasi fungsi custom_release
        return 0;
    }

    static int custom_getxattr(const char *file_path, const char *name, char *value, size_t size) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int result = lgetxattr(full_path, name, value, size);
        if (result == -1) return -errno;

        return result;
    }

    static int custom_setxattr(const char *file_path, const char *name, const char *value, size_t size, int flags) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0)
        {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int result = lsetxattr(full_path, name, value, size, flags);
        if (result == -1) return -errno;
        return 0;
    }

    static int custom_truncate(const char *path, off_t size) {
        char full_path[9999];
        sprintf(full_path, "%s%s", myPath, path);

        int result = truncate(full_path, size);
        if (result == -1) return -errno;

        return 0;
    }

    static int custom_ftruncate(const char *path, off_t size, struct fuse_file_info *fi) {
        char full_path[9999];
        sprintf(full_path, "%s%s", myPath, path);

        int result = ftruncate(fi->fh, size);
        if (result == -1) return -errno;

        return 0;
    }

    static struct fuse_operations custom_operations = {
        .getattr = custom_getattr,
        .access = custom_access,
        .readdir = custom_readdir,
        .read = custom_read,
        .truncate = custom_truncate,
        .ftruncate = custom_ftruncate,
        .open = custom_open,
        .write = custom_write,
        .mkdir = custom_mkdir,
        .create = custom_create,
        .unlink = custom_unlink,
        .rmdir = custom_rmdir,
        .rename = custom_rename,
        .flush = custom_flush,
        .release = custom_release,
        .setxattr = custom_setxattr,
        .getxattr = custom_getxattr,
    };

    int main(int argc, char *argv[]) {
        umask(0);
        return fuse_main(argc, argv, &custom_operations, NULL);
    }
