#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

static const char *gallery_path = "/gallery";
static const char *rev_folder_name = "rev.";
static const char *delete_folder_name = "delete.";

void reverse_string(char *str) {
    int i = 0, j = strlen(str) - 1;
    while (i < j) {
        char temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++;
        j--;
    }
}

void create_rev_folder() {
    char rev_path[1024];
    sprintf(rev_path, "%s%s", gallery_path, rev_folder_name);
    mkdir(rev_path, 0755); 
}

void create_delete_folder() {
    char delete_path[1024];
    sprintf(delete_path, "%s%s", gallery_path, delete_folder_name);
    mkdir(delete_path, 0755); 
}

void delete_file(const char *path) {
    unlink(path);
}

static int hello_mkdir(const char *path, mode_t mode) {
    char rev_path[1024];
    char delete_path[1024];
    
    if (strstr(path, gallery_path) == path && strcmp(path, gallery_path) != 0) {
        
        sprintf(rev_path, "%s%s%s", gallery_path, path, rev_folder_name);
        mkdir(rev_path, mode);

        sprintf(delete_path, "%s%s%s", gallery_path, path, delete_folder_name);
        mkdir(delete_path, mode);
    }
    return 0;
}

static int hello_rename(const char *from, const char *to) {
    char rev_path[1024], rev_filename[256];
    char delete_path[1024], delete_filename[256];
    
    if (strstr(to, rev_folder_name) == to) {

        sprintf(rev_path, "%s%s", gallery_path, to);
        strcpy(rev_filename, rev_path + strlen(rev_path) - strlen(rev_folder_name));
        reverse_string(rev_filename);
        sprintf(rev_path, "%s%s", gallery_path, rev_filename);
        
        rename(from, rev_path);
    } else if (strstr(to, delete_folder_name) == to) {

        delete_file(from);
    }
    return 0;
}

static struct fuse_operations hello_oper = {
    .mkdir = hello_mkdir,
    .rename = hello_rename,
};

int main(int argc, char *argv[]) {

    create_rev_folder();

    create_delete_folder();

    return fuse_main(argc, argv, &hello_oper, NULL);
}

//Revisi

#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>  

static const char *gallery_path = "/gallery";
static const char *rev_folder_name = "rev.";

void reverse_string(char *str);  

void create_rev_folder() {
    char rev_path[1024];
    sprintf(rev_path, "%s%s", gallery_path, rev_folder_name);
    mkdir(rev_path, 0755);
}

static int hello_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {

    DIR *dp;
    struct dirent *de;

    (void) offset;
    (void) fi;

    char gallery_full_path[1024];
    sprintf(gallery_full_path, "%s%s", gallery_path, path);

    dp = opendir(gallery_full_path);

    if (dp == NULL) {

        perror("opendir");
        return -errno;  
    }

    while ((de = readdir(dp)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char rev_filename[256];
        strcpy(rev_filename, de->d_name);
        reverse_string(rev_filename);

        char rev_path[1024];
        sprintf(rev_path, "%s%s%s", gallery_path, rev_folder_name, rev_filename);

        if (filler(buf, de->d_name, &st, 0)) {

            break;
        }
    }

    closedir(dp);

    return 0;
}

void reverse_string(char *str) {

    int i = 0, j = strlen(str) - 1;
    while (i < j) {
        char temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++;
        j--;
    }
}

static struct fuse_operations hello_oper = {
    .readdir = hello_readdir,
};

int main(int argc, char *argv[]) {
    create_rev_folder();

    return fuse_main(argc, argv, &hello_oper, NULL);

}


