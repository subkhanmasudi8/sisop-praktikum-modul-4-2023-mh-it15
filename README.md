# sisop-praktikum-modul-4-2023-MH-IT15
Laporan Resmi pengerjaan soal shift modul 4 Praktikum Sistem Operasi 2023 Kelompok IT15

## Anggota Kelompok
1. Ilhan Ahmad Syafa (5027221040)
2. Subkhan Masudi (5027221044)
3. Gilang Raya Kurniawan (5027221045)

# SOAL 1

Anthoni Salim merupakan seorang pengusaha yang memiliki supermarket terbesar di Indonesia. Ia sedang melakukan inovasi besar dalam dunia bisnis ritelnya. Salah satu ide cemerlang yang sedang dia kembangkan adalah terkait dengan pengelolaan foto dan gambar produk dalam sistem manajemen bisnisnya. Anthoni bersama sejumlah rekan bisnisnya sedang membahas konsep baru yang akan mengubah cara produk-produk di supermarketnya dipresentasikan dalam katalog digital. Untuk resource dapat di download pada link ini.

## Program Soal 1

- Pada folder “gallery”, agar katalog produk lebih menarik dan kreatif, Anthoni dan tim memutuskan untuk:

        1. Membuat folder dengan prefix "rev." Dalam folder    ini, setiap gambar yang dipindahkan ke dalamnya akan mengalami pembalikan nama file-nya. Ex: "mv EBooVNhNe7tU7q08jgTe.HEIC rev-test/" Output: eTgj80q7Ut7eNhNVooBE.HEIC
        2. Anthoni dan timnya ingin menghilangkan gambar-gambar produk yang sudah tidak lagi tersedia dengan membuat folder dengan prefix "delete." Jika sebuah gambar produk dipindahkan ke dalamnya, nama file-nya akan langsung terhapus. Ex: "mv coba-deh.jpg delete-foto/" 


- Pada folder "sisop," terdapat file bernama "script.sh." Anthoni dan timnya menyadari pentingnya menjaga keamanan dan integritas data dalam folder ini. 

        1. Mereka harus mengubah permission pada file "script.sh" karena jika dijalankan maka dapat menghapus semua dan isi dari  "gallery," "sisop," dan "tulisan."
        1. Anthoni dan timnya juga ingin menambahkan fitur baru dengan membuat file dengan prefix "test" yang ketika disimpan akan mengalami pembalikan (reverse) isi dari file tersebut.  


-  Pada folder "tulisan" Anthoni ingin meningkatkan kemampuan sistem mereka dalam mengelola berkas-berkas teks dengan menggunakan fuse.

1.         Jika sebuah file memiliki prefix "base64," maka sistem akan langsung mendekode isi file tersebut dengan algoritma Base64.

1.         Jika sebuah file memiliki prefix "rot13," maka isi file tersebut akan langsung di-decode dengan algoritma ROT13.

1.         Jika sebuah file memiliki prefix "hex," maka isi file tersebut akan langsung di-decode dari representasi heksadesimalnya.

1.         Jika sebuah file memiliki prefix "rev," maka isi file tersebut akan langsung di-decode dengan cara membalikkan teksnya.


- Pada folder “disable-area”, Anthoni dan timnya memutuskan untuk menerapkan kebijakan khusus. Mereka ingin memastikan bahwa folder dengan prefix "disable" tidak dapat diakses tanpa izin khusus. 


1.          Jika seseorang ingin mengakses folder dan file pada “disable-area”, mereka harus memasukkan sebuah password terlebih dahulu (password bebas). 


- Setiap proses yang dilakukan akan tercatat pada logs-fuse.log dengan format :
[SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information]
Ex: [SUCCESS]::01/11/2023-10:43:43::rename::Move from /gallery/DuIJWColl2UYknZ8ubz6.HEIC to /gallery/foto DuIJWColl2UYknZ8ubz6

## Solution Soal 1
[Source Code Soal 1](https://gitlab.com/subkhanmasudi8/sisop-praktikum-modul-4-2023-mh-it15/-/blob/main/soal_1/hell.c?ref_type=heads)

## penjelasan Soal 1 

Pertama tama buat program dengan nama hell.c, lalu download dahulu file yang diberikan pada soal yaiut dari link (https://drive.google.com/file/d/1ci9afRbwJi-jrly7CB7HiC40rsuvSa03/view) ekstrak file tersebut, dan lettakan isinya dimana folder hell.c berada

```
#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>  

static const char *gallery_path = "/gallery";
static const char *rev_folder_name = "rev.";

void reverse_string(char *str);  

void create_rev_folder() {
    char rev_path[1024];
    sprintf(rev_path, "%s%s", gallery_path, rev_folder_name);
    mkdir(rev_path, 0755);
}

static int hello_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {

    DIR *dp;
    struct dirent *de;

    (void) offset;
    (void) fi;

    char gallery_full_path[1024];
    sprintf(gallery_full_path, "%s%s", gallery_path, path);

    dp = opendir(gallery_full_path);

    if (dp == NULL) {

        perror("opendir");
        return -errno;  
    }

    while ((de = readdir(dp)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char rev_filename[256];
        strcpy(rev_filename, de->d_name);
        reverse_string(rev_filename);

        char rev_path[1024];
        sprintf(rev_path, "%s%s%s", gallery_path, rev_folder_name, rev_filename);

        if (filler(buf, de->d_name, &st, 0)) {

            break;
        }
    }

    closedir(dp);

    return 0;
}

void reverse_string(char *str) {

    int i = 0, j = strlen(str) - 1;
    while (i < j) {
        char temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++;
        j--;
    }
}

static struct fuse_operations hello_oper = {
    .readdir = hello_readdir,
};

int main(int argc, char *argv[]) {
    create_rev_folder();

    return fuse_main(argc, argv, &hello_oper, NULL);

}
```

Pertama, pada program diatas pada line `#define FUSE_USE_VERSION 30`, didefinisikan versi **FUSE yang akan digunakan. Yaitu versi 30**
Selanjutnya  program mendefinisikan dua variabel statis, yaitu `gallery_path` dan `rev_folder_name`, yang menentukan **path dari (gallery)** dan nama **folder untuk file-file yang akan di-reverse yang dimana adalah rev**. Dilanjutkan deklarasi fungsi `reverse_string` yang akan digunakan untuk membalikkan string yang diberikan. Berikutnya, terdapat `create_rev_folder` yang **berfungsi membuat folder rev di dalam folder gallery**. Fungsi ini dipanggil dalam fungsi `main` **sebelum FUSE filesystem dijalankan**. Fungsi `hello_readdir` merupakan **impletasi dari operasi readdir dalam fungsi FUSE**. Fungsi ini **digunakan ketika isi dari suatu direktori diminta (misal folder gallery/rev)**. Di mana, program membuka direktori yang diminta dan **membaca setiap entri, menghasilkan nama file yang sudah di-reverse**. Fungsi `reverse_string` digunakan untuk membalikkan nama sebuat file. Didefinisikan struktur `fuse_operations` yang **mengaitkan fungsi `readdir` dengan fungsi `hello_readdir`. Di akhir dalam fungsi `main`**, program pertama tama memanggil `create_rev_folder` untuk membuat **folder reverse, kemudian menjalankan FUSE filesystem menggunakan `fuse_main`.**

## Pengerjaan Soal 1

Pertama tama buat program hell.c terlebih dahulu lalu compile hell.c tersebut. gunakan **-lfuse** karena digunakan fungsi fuse
<img src="https://i.ibb.co/VgNjHM0/p3.png" alt="p3" border="0">

Lalu  jalankan program hell.c, karena menggunakan fuse mount ke folder gallery/rev untuk mereverse nama file
<img src="https://i.ibb.co/qk4Y3q6/p2.png" alt="p2" border="0">

Lalu check terlebih dahulu apakah folder rev sudah ter mount atau beluum dengan menggunakan command **mount -l**
<img src="https://i.ibb.co/4TfbJd1/p1.png" alt="p1" border="0">

## Kendala Soal 1

- Program hanya dapat melakukan fungsi mount **hampir semua problem yang ada di soal tidak bisa / belum bisa**.


# SOAL 2

Manda adalah seorang mahasiswa IT, dimana ia merasa hari ini adalah hari yang menyebalkan karena sudah bertemu lagi dengan praktikum sistem operasi. Pada materi hari ini, ia mempelajari suatu hal bernama FUSE. Karena sesi lab telah selesai, kini waktunya untuk mengerjakan penugasan praktikum. Manda mendapatkan firasat jika soal modul kali ini adalah yang paling sulit dibandingkan modul lainnya, sehingga dia akan mulai mengerjakan tugas praktikum-nya. Sebelumnya, Manda mendapatkan file yang perlu didownload secara manual terlebih dahulu pada link ini. Selanjutnya, ia tinggal mengikuti langkah - langkah yang diminta untuk mengerjakan soal ini hingga akhir. Manda berharap ini menjadi modul terakhir sisop yang ia pelajari

## Problem Soal 2

A. Membuat file **open-password.c** untuk membaca file **zip-pass.txt**
- Melakukan proses dekripsi base64 terhadap file tersebut
- Lalu unzip **home.zip** menggunakan password hasil dekripsi


B. Membuat file **semangat.c**
- Setiap proses yang dilakukan akan tercatat pada **logs-fuse.log** dengan format dan contoh sebagai berikut :

`[SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information]`

`[SUCCESS]::06/11/2023-16:05:48::readdir::Read directory /sisop`

- Selanjutnya untuk memudahkan pengelolaan file, Manda diminta untuk mengklasifikasikan file sesuai jenisnya:
  - Melakukan unzip pada **home.zip**
  - Membuat folder yang dapat langsung memindahkan file dengan kategori ekstensi file yang mereka tetapkan:
    - documents: .pdf dan .docx. 
    - images: .jpg, .png, dan .ico. 
    - website: .js, .html, dan .json. 
    - sisop: .c dan .sh. 
    - text: .txt. 
    - aI: .ipynb dan .csv.

- Karena sedang belajar tentang keamanan, tiap kali mengakses file (cat nama-file) pada di dalam folder **text** maka perlu memasukkan password terlebih dahulu
  - Password terdapat di file **password.bin**

- Pada folder **website**
  - Membuat file csv pada folder website dengan format : `file,title,body`
  - Tiap kali membaca file csv dengan format yang telah ditentukan, maka akan langsung tergenerate sebuah file html sejumlah yang telah dibuat pada file csv

- Tidak dapat menghapus file / folder yang mengandung prefix **“restricted”**
- Pada folder **documents**
  - Karena ingin mencatat hal - hal penting yang mungkin diperlukan, maka Manda diminta untuk menambahkan detail - detail kecil dengan memanfaatkan attribut

C. Membuat file **server.c**
  - Pada folder ai, terdapat file **webtoon.csv**
  - Manda diminta untuk membuat sebuah server (socket programming) untuk membaca **webtoon.csv**. Dimana terjadi pengiriman data antara client ke server dan server ke client.
    - Menampilkan seluruh judul
    - Menampilkan berdasarkan genre
    - Menampilkan berdasarkan hari
    - Menambahkan ke dalam file webtoon.csv
    - Melakukan delete berdasarkan judul
    - Selain command yang diberikan akan menampilkan tulisan **“Invalid Command”**
Manfaatkan **client.c** pada folder sisop sebagai client

## Solution Soal 2
[Source Code Soal 2](https://gitlab.com/subkhanmasudi8/sisop-praktikum-modul-4-2023-mh-it15/-/tree/main/soal_2?ref_type=heads)

### Solution Soal 2 A
Langkah pertama, download terlebih dahulu **files.zip** pada link <a href="https://drive.google.com/file/d/1MFQ6ds9lpbo9I6-QlXjyFPYyMEZpB9Tq/view?usp=drive_link" target="_blank">berikut.</a>

Kemudian, unzip **files.zip**

Kemudian, buat file **open-password.c** dengan isi code berikut.
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char byte;

byte get_number_b64_from_symbol_0_64(byte b) {
    if (b >= 0x41 && b <= 0x5a)
        return b - 0x41;
    if (b >= 0x61 && b <= 0x7a)
        return b - 0x47;
    if (b >= 0x30 && b <= 0x39)
        return b + 0x4;
    if (b == 0x2b)
        return 0x3e;
    if (b == 0x2f)
        return 0x3f;
    if (b == 0x3d)
        return 0xff;
    return 0xff;
}

byte *from_b64_bytes(char b64[]) {
    if (strlen(b64) % 4 != 0)
        return NULL;

    byte *ret = (byte *)malloc(1);
    ret[0] = '\0';

    for (int i = 0; i < strlen(b64); i += 4) {
        byte b_ = get_number_b64_from_symbol_0_64(b64[i]) << 2;
        b_ += get_number_b64_from_symbol_0_64(b64[i + 1]) >> 4;
        ret = (byte *)realloc(ret, strlen(ret) + 2); 
        sprintf((char *)ret, "%s%c", (char *)ret, b_);

        if (get_number_b64_from_symbol_0_64(b64[i + 2]) == 0xff)
            break;

        b_ = get_number_b64_from_symbol_0_64(b64[i + 1]) << 4;
        b_ += get_number_b64_from_symbol_0_64(b64[i + 2]) >> 2;
        ret = (byte *)realloc(ret, strlen(ret) + 2);
        sprintf((char *)ret, "%s%c", (char *)ret, b_);

        if (get_number_b64_from_symbol_0_64(b64[i + 3]) == 0xff)
            break;

        b_ = get_number_b64_from_symbol_0_64(b64[i + 2]) << 6;
        b_ += get_number_b64_from_symbol_0_64(b64[i + 3]);
        ret = (byte *)realloc(ret, strlen(ret) + 2);
        sprintf((char *)ret, "%s%c", (char *)ret, b_);
    }
    return ret;
}

int main() {
    FILE *file = fopen("zip-pass.txt", "r");
    if (file == NULL) {
        perror("Error opening file");
        return 1;
    }

    char encodedPassword[0xff];
    if (fgets(encodedPassword, sizeof(encodedPassword), file) == NULL) {
        perror("Error reading password from file");
        fclose(file);
        return 1;
    }

    encodedPassword[strcspn(encodedPassword, "\n")] = 0;

    fclose(file);

    byte *decodedText = from_b64_bytes(encodedPassword);

    if (decodedText != NULL) {
        printf("Decoded text: \"%s\"\n", decodedText);
        free(decodedText);
    } else {
        printf("Invalid Base64 input\n");
    }

    return 0;
}

```
  - Definisikan tipe data `byte` sebagai unsigned char yang mana akan digunakan untuk menyimpan byte dari data biner.

  - Fungsi `byte get_number_b64_from_symbol_0_64(byte b)` mengonversi karakter dalam basis64 menjadi nilai numerik. Base64 menggunakan huruf besar (A-Z), huruf kecil (a-z), angka (0-9), dan dua karakter khusus ('+', '/'). Fungsi ini mengembalikan nilai numerik sesuai dengan aturan tertentu.

  - Fungsi `byte *from_b64_bytes(char b64[])` mengonversi string Base64 menjadi data biner. Jika panjang string Base64 tidak habis dibagi 4, maka fungsi ini mengembalikan NULL. Fungsi ini juga menginisialisasi string ret yang akan menyimpan hasil konversi.

  - Loop untuk mengonversi setiap blok 4 karakter Base64 menjadi 3 byte data biner. Operasi-operasi ini menggunakan hasil dari fungsi `get_number_b64_from_symbol_0_64` dan mengubahnya menjadi bentuk biner.

  - Percabangan if untuk menghandle karakter ketiga dan keempat dari blok Base64. Jika karakter ini adalah karakter padding (0xff), maka loop dihentikan. Jika tidak, maka operasi konversi dilanjutkan. Setelah loop selesai, fungsi mengembalikan data biner yang sudah dikonversi.

  - Fungsi `main` membuka file **zip-pass.tx** untuk dibaca. Jika gagal, program memberikan pesan kesalahan dan keluar dengan status 1. Program membaca baris pertama dari file, yang berisi string Base64 yang akan didecode. Program menutup file setelah membaca string Base64. Kemudian, program memanggil fungsi `from_b64_bytes` untuk mendecode string Base64 menjadi data biner. Hasilnya dicetak ke layar. Jika Base64 tidak valid, program memberikan pesan kesalahan. Program mengembalikan status 0 untuk menandakan bahwa program telah berakhir dengan sukses.

Kemudian, compile dan jalankan file **open-password.c**

Setelah mendapatkan encoded passwordnya, maka unzip file **home.zip** dengan command ```unzip -P F#a4xPZS.,jfgUk#nT@l home.zip```

### Solution Soal 2 B
### Solution Soal 2 C
Buat file **server.c** dengan isi code berikut
```
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

#define PORT 8080
#define MAX_BUFFER_SIZE 1024
#define WEBTOON_FILE_PATH "webtoon.csv"

void send_title_with_number(int new_socket, int number, const char* title) {
    char response[MAX_BUFFER_SIZE];
    sprintf(response, "%d. %s", number, title);
    send(new_socket, response, strlen(response), 0);
}

void handle_client_request(int new_socket) {
    char buffer[MAX_BUFFER_SIZE] = {0};
    FILE *webtoon_file;
    char line[MAX_BUFFER_SIZE];

    webtoon_file = fopen(WEBTOON_FILE_PATH, "r+");
    if (webtoon_file == NULL) {
        perror("Error opening webtoon.csv");
        exit(EXIT_FAILURE);
    }

    while (1) {
        read(new_socket, buffer, sizeof(buffer));

        printf("Received: %s\n", buffer);

        int titles_found = 0; 

        if (strncmp(buffer, "list all", 8) == 0) {
            int number = 1;
            fseek(webtoon_file, 0, SEEK_SET);  
            while (fgets(line, sizeof(line), webtoon_file) != NULL) {
                send_title_with_number(new_socket, number, line);
                number++;
                titles_found = 1;
            }
            if (!titles_found) {
                send(new_socket, "Maaf, tidak ada judul webtoons yang sesuai.", sizeof("Maaf, tidak ada judul webtoons yang sesuai."), 0);
            } else {
                send(new_socket, "Listed all webtoons.", sizeof("Listed all webtoons."), 0);
            }
        } else if (strncmp(buffer, "genre", 5) == 0) {
            char genre[20];
            sscanf(buffer, "genre %s", genre);

            int number = 1;
            fseek(webtoon_file, 0, SEEK_SET);  
            while (fgets(line, sizeof(line), webtoon_file) != NULL) {
                if (strstr(line, genre) != NULL) {
                    send_title_with_number(new_socket, number, line);
                    number++;
                    titles_found = 1;
                }
            }
        } else if (strncmp(buffer, "hari", 4) == 0) {
            char day[20];
            sscanf(buffer, "hari %s", day);

            int number = 1;
            fseek(webtoon_file, 0, SEEK_SET); 
            while (fgets(line, sizeof(line), webtoon_file) != NULL) {
                if (strstr(line, day) != NULL) {
                    send_title_with_number(new_socket, number, line);
                    number++;
                    titles_found = 1;
                }
            }
        } else if (strncmp(buffer, "add", 3) == 0) {
            char title[50], genre[20], day[20];
            sscanf(buffer, "add %s %s %s", day, genre, title);

            fprintf(webtoon_file, "%s,%s,%s\n", day, genre, title);
            fflush(webtoon_file);

            send(new_socket, "Entry added successfully.", sizeof("Entry added successfully."), 0);
        } else if (strncmp(buffer, "delete", 6) == 0) {
            char title[50];
            sscanf(buffer, "delete %s", title);

            FILE *temp_file = fopen("webtoon_temp.csv", "w");
            if (temp_file == NULL) {
                perror("Error creating temporary file");
                exit(EXIT_FAILURE);
            }

            fseek(webtoon_file, 0, SEEK_SET);
            while (fgets(line, sizeof(line), webtoon_file) != NULL) {
                if (strstr(line, title) == NULL) {
                    fprintf(temp_file, "%s", line);
                }
            }

            fclose(webtoon_file);
            fclose(temp_file);

            remove(WEBTOON_FILE_PATH);
            rename("webtoon_temp.csv", WEBTOON_FILE_PATH);

            send(new_socket, "Entry deleted successfully.", sizeof("Entry deleted successfully."), 0);

            webtoon_file = fopen(WEBTOON_FILE_PATH, "r+");
            if (webtoon_file == NULL) {
                perror("Error opening updated webtoon.csv");
                exit(EXIT_FAILURE);
            }
        } else if (strncmp(buffer, "exit", 4) == 0) {
            break;
        } else {
            send(new_socket, "Invalid Command", sizeof("Invalid Command"), 0);
        }

        if (!titles_found) {
            send(new_socket, "Maaf, tidak ada judul webtoons yang sesuai.", sizeof("Maaf, tidak ada judul webtoons yang sesuai."), 0);
        }

        memset(buffer, 0, sizeof(buffer));
    }

    fclose(webtoon_file);
    close(new_socket);
}

int main() {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    while (1) {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        if (fork() == 0) {
            close(server_fd);
            handle_client_request(new_socket);
            exit(0);
        } else {
            close(new_socket);
        }
    }

    return 0;
}

```
Tambahkan library yang dibutuhkan dan define port yang hendak digunakan untuk socket programming (sama dengan port yang digunakan client) serta define path dari file **webtoon.csv**.

Fungsi `send_title_with_number()` digunakan untuk merapikan list yang ditampilkan dengan melakukan penomoran pada tiap listnya.

Fungsi `handle_client_request()` digunakan untuk menghandle dan menghubungkan server dengan client serta membaca file **webtoon.csv**. Di dalamnya terdapat percabangan sesuai dengan requirements pada soal. Pertama, ada kondisi `list all` yang akan memunculkan semua list pada file **webtoon.csv**. Kedua, kondisi ketika client menginput `genre (nama_genre)` maka program akan mencari line pada file **webtoon.csv** yang mengandung kata nama genre yang diinput dan menampilkannya. Ketiga, kondisi ketika client menginput `hari (nama_hari)` maka program akan mencari line pada file **webtoon.csv** yang mengandung kata nama hari yang diinput dan menampilkannya. Keempat, kondisi `add (day) (genre) (title)` akan menambahkan line baru sesuai inputan client ke dalam file **webtoon.csv**. Kelima, kondisi `delete (title)` akan menghapus line pada file **webtoon.csv** yang mengandung judul sesuai dengan inputan client. Keenam, kondisi `exit` maka program akan berhenti sejenak atau melakukan break. Jika command yang diinput client tidak sesuai dengan kondisi percabangan-percabangan di atas, maka program akan memunculkan pesan **Invalid Command**. Berlaku juga jika judul webtoons yang diinput oleh client tidak sesuai dengan list yang ada pada file **webtoons.csv** (kecuali command `add` dan `list all`).

Fungsi `main()` berisi konfigurasi untuk connect dengan client dan juga memanggil fungsi-fungsi yang telah dibuat sebelumnya.

## Pengerjaan Soal 2
Download dan unzip file **files.zip**

<a href="https://ibb.co/Y7nNk52"><img src="https://i.ibb.co/vxycPN1/unzip1.png" alt="unzip1" border="0" /></a>

Jalankan file **open-password.c**

<a href="https://ibb.co/b1F24Vp"><img src="https://i.ibb.co/6Y81KS6/run-open-password.png" alt="run-open-password" border="0" /></a>

Unzip **home.zip** 

<a href="https://ibb.co/7vr8JbJ"><img src="https://i.ibb.co/XXFNZVZ/unzip2.png" alt="unzip2" border="0" /></a>

Compile dan jalankan masing-masing program **server** dan **client**

List All

<a href="https://ibb.co/SwNz76R"><img src="https://i.ibb.co/r2kVfQ4/listall-1.png" alt="listall-1" border="0" /></a> 
<a href="https://ibb.co/vjT0VvT"><img src="https://i.ibb.co/pLMYbrM/listall-2.png" alt="listall-2" border="0" /></a>

Add

<a href="https://ibb.co/1JFV5Zw"><img src="https://i.ibb.co/gFk1pyB/add1.png" alt="add1" border="0" /></a>
<a href="https://ibb.co/C2ySKq4"><img src="https://i.ibb.co/fvWTdbZ/add2.png" alt="add2" border="0" /></a>

Delete 

<a href="https://ibb.co/GW62LDs"><img src="https://i.ibb.co/fkgp5W8/delete1.png" alt="delete1" border="0" /></a>
<a href="https://ibb.co/fNycwZb"><img src="https://i.ibb.co/T07C3JZ/delete2.png" alt="delete2" border="0" /></a>

Genre

<a href="https://ibb.co/QnXXV0G"><img src="https://i.ibb.co/tc336R5/genre.png" alt="genre" border="0" /></a>

Hari

<a href="https://ibb.co/M6WCjwm"><img src="https://i.ibb.co/C2NHXCf/hari.png" alt="hari" border="0" /></a>

Invalid Command dan Invalid Judul Webtoons

<a href="https://ibb.co/3yWpsnd"><img src="https://i.ibb.co/PZ4cYdj/invalid.png" alt="invalid" border="0" /></a> 


## Kendala Soal 2
Implementasi penggunaan fuse pada soal 2B yaitu file **semangat.c**

## Problem Soal 3

a. Pada filesystem tersebut, jika User membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 
b. Jika sebuah direktori adalah direktori modular, maka akan dilakukan modularisasi pada folder tersebut dan juga sub-direktorinya.
c. Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan berikut:
    -Log akan dibagi menjadi beberapa level, yaitu REPORT dan FLAG.
    -Pada level log FLAG, log akan mencatat setiap kali terjadi system call rmdir (untuk menghapus direktori) dan unlink (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log REPORT.
    -Format untuk logging yaitu sebagai berikut.
[LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]

**Contoh:**
REPORT::231105-12:29:28::RENAME::/home/sarah/selfie.jpg::/home/sarah/cantik.jpg
REPORT::231105-12:29:33::CREATE::/home/sarah/test.txt
FLAG::231105-12:29:33::RMDIR::/home/sarah/folder

D. Saat dilakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.

**Contoh:**
file File_Sarah.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Sarah.txt.000, File_Sarah.txt.001, dan File_Sarah.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).
Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).

**Contoh :**
/tmp/test_fuse/ adalah filesystem yang harus dirancang.
/home/index/modular/ adalah direktori asli.

## Solution Soal 3

[Source Code Soal 3](https://gitlab.com/subkhanmasudi8/sisop-praktikum-modul-4-2023-mh-it15/-/tree/main/soal_3?ref_type=heads)

Code yang telah saya berikan sejauh ini sudah mencakup sebagian besar fungsi-fungsi dasar yang diperlukan untuk membuat filesystem dengan persyaratan yang telah disebutkan dalam tugas. Beberapa fungsi utama yang sudah termasuk dalam kode saya antara lain:

1. **custom_getattr:** Mendapatkan atribut dari file atau direktori.
2. **custom_read:** Membaca isi file.
3. **custom_readdir:** Membaca isi direktori.
4. **custom_open:** Membuka file.
5. **custom_write:** Menulis ke file.
6. **custom_mkdir:** Membuat direktori.
7. **custom_access:** Mengakses file atau direktori.
8. **custom_create:** Membuat file.
9. **custom_rename:** Mengganti nama file atau direktori.
10. **custom_unlink:** Menghapus file.
11. **custom_rmdir:** Menghapus direktori.
12. **custom_flush dan custom_release:** Fungsi-fungsi untuk pengelolaan buffer dan penutupan file.

## Solution Soal 3A

Dengan menggunakan program ini, dapat membuat sistem file yang mengenali direktori dengan awalan "module_" sebagai direktori modular.

```
#define FUSE_USE_VERSION 30
#define _FILE_OFFSET_BITS 64

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

static const char *modular_prefix = "module_";

static int myfs_getattr(const char *path, struct stat *stbuf)
{
    int res = 0;

    memset(stbuf, 0, sizeof(struct stat));

    if (strncmp(path, modular_prefix, strlen(modular_prefix)) == 0) {
        // If the directory has the specified prefix, consider it modular
        stbuf->st_mode = S_IFDIR | 0755;
    } else {
        // Otherwise, treat it as a regular directory
        stbuf->st_mode = S_IFDIR | 0777;
    }

    return res;
}

static int myfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    (void)offset;
    (void)fi;

    if (strcmp(path, "/") != 0) {
        return -ENOENT;
    }

    filler(buf, ".", NULL, 0);
    filler(buf, "..", NULL, 0);

    // List modular directories
    filler(buf, modular_prefix, NULL, 0);

    return 0;
}

static struct fuse_operations myfs_operations = {
    .getattr = myfs_getattr,
    .readdir = myfs_readdir,
};

int main(int argc, char *argv[])
{
    return fuse_main(argc, argv, &myfs_operations, NULL);
}

```


## Solution Soal 3B

Jika sebuah direktori adalah direktori modular, maka akan dilakukan modularisasi pada folder tersebut dan juga sub-direktorinya.

```
#include <dirent.h>

// ... (kode sebelumnya)

static void modularize_directory(const char *path)
{
    DIR *dir;
    struct dirent *entry;

    dir = opendir(path);
    if (dir == NULL) {
        perror("opendir");
        return;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) {
            // Skip "." and ".."
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                continue;
            }

            // Construct full path for the subdirectory
            char sub_path[PATH_MAX];
            snprintf(sub_path, sizeof(sub_path), "%s/%s", path, entry->d_name);

            // Modularize the subdirectory
            modularize_directory(sub_path);
        }
    }

    closedir(dir);
}

static int myfs_mkdir(const char *path, mode_t mode)
{
    int res = mkdir(path, mode);

    if (res == 0 && strncmp(path, modular_prefix, strlen(modular_prefix)) == 0) {
        // If the directory has the specified prefix, modularize it and its subdirectories
        modularize_directory(path);
    }

    return res;
}

static int myfs_rename(const char *oldpath, const char *newpath)
{
    int res = rename(oldpath, newpath);

    if (res == 0) {
        // If renaming to a name with the specified prefix, modularize the directory and its subdirectories
        if (strncmp(newpath, modular_prefix, strlen(modular_prefix)) == 0) {
            modularize_directory(newpath);
        }
    }

    return res;
}

// ... (fungsi-fungsi lainnya tetap sama)

static struct fuse_operations myfs_operations = {
    .getattr = myfs_getattr,
    .readdir = myfs_readdir,
    .mkdir = myfs_mkdir,
    .rename = myfs_rename,
};

// ... (kode setelahnya tetap sama)


```
## Solution Soal 3C

```
    #define HAVE_SETXATTR
    #define FUSE_USE_VERSION 28
    #define _XOPEN_SOURCE 500
    #include <fuse.h>
    #include <string.h>
    #include <time.h>
    #include <unistd.h>
    #include <stdio.h>
    #include <sys/time.h>
    #include <sys/xattr.h>
    #include <fcntl.h>
    #include <dirent.h>
    #include <errno.h>

    static const char *myPath = "/home/dimas/sisopmodul4";
    static const char *myLogFile = "/home/dimas/sisopmodul4/fs_module.log";

    void custom_logger(const char *file_path, const char *action, const char *log_type) {
        char current_time[30];
        char report[9999];
        struct tm *time_info;
        time_t now;

        time(&now);
        time_info = localtime(&now);

        strftime(current_time, sizeof(current_time), "%y%m%d-%H:%M:%S", time_info);
        sprintf(report, "%s::%s::%s::%s\n", log_type, current_time, action, file_path);
        FILE *log_file = fopen(myLogFile, "a");

        fprintf(log_file, "%s", report);
        printf("> Logged\n");

        fclose(log_file);
    }

    static int custom_getattr(const char *file_path, struct stat *stat_buffer) {
        int result;
        char full_path[9999];
        sprintf(full_path, "%s%s", myPath, file_path);
        result = lstat(full_path, stat_buffer);
        if (result == -1) return -errno;
        return 0;
    }

    static int custom_read(const char *file_path, char *buffer, size_t size, off_t offset, struct fuse_file_info *file_info) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int result = 0, file_descriptor = 0;
        (void) file_info;

        file_descriptor = open(full_path, O_RDONLY);
        if (file_descriptor == -1) return -errno;

        result = pread(file_descriptor, buffer, size, offset);
        if (result == -1) return -errno;

        close(file_descriptor);
        return result;
    }

    static int custom_readdir(const char *file_path, void *buffer, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *file_info) {
        char full_path[9999];
        char new_name[256];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        DIR *directory;
        struct dirent *dir_entry;
        (void) offset;
        (void) file_info;

        int result = 0;

        directory = opendir(full_path);
        if (directory == NULL) return -errno;

        while ((dir_entry = readdir(directory)) != NULL) {
            struct stat file_stat;
            memset(&file_stat, 0, sizeof(file_stat));

            file_stat.st_ino = dir_entry->d_ino;
            file_stat.st_mode = dir_entry->d_type << 12;

            strcpy(new_name, dir_entry->d_name);
            if (!(strcmp(new_name, ".") == 0 || strcmp(new_name, "..") == 0) && strstr(full_path, "module_")) {
                printf("%s berada pada modular dir\n", new_name);
            } else printf("%s tidak berada pada modular dir\n", new_name);

            result = (filler(buffer, new_name, &file_stat, 0));

            if (result != 0) break;
        }

        closedir(directory);
        return 0;
    }

    static int custom_open(const char *file_path, struct fuse_file_info *file_info) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int result = open(full_path, file_info->flags);
        if (result == -1) return -errno;

        file_info->fh = result;
        return 0;
    }

    static int custom_write(const char *file_path, const char *buffer, size_t size, off_t offset, struct fuse_file_info *file_info) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int file_handle = open(full_path, O_WRONLY);
        if (file_handle == -1) return -errno;
        int result = pwrite(file_handle, buffer, size, offset);
        if (result == -1) return -errno;
        close(file_handle);
        return result;
    }

    static int custom_mkdir(const char *file_path, mode_t mode) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int result = mkdir(full_path, mode);
        if (result == -1) return -errno;

        custom_logger(full_path, "MKDIR", "REPORT");
        return 0;
    }

    static int custom_access(const char *file_path, int mask) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int result = access(full_path, mask);
        if (result == -1) return -errno;
        return 0;
    }

    static int custom_create(const char *file_path, mode_t mode, struct fuse_file_info *file_info) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        (void) file_info;

        int result = creat(full_path, mode);
        if (result == -1) return -errno;

        close(result);
        custom_logger(full_path, "CREATE", "REPORT");
        return 0;
    }

    static int custom_rename(const char *from, const char *to) {
        int result = 0;
        char report[9999];

        result = rename(from, to);
        if (result == -1) return -errno;

        sprintf(report, "%s::%s", from, to);
        custom_logger(report, "RENAME", "REPORT");
        return 0;
    }

    static int custom_unlink(const char *file_path) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int result = unlink(full_path);
        if (result == -1) return -errno;

        custom_logger(full_path, "UNLINK", "FLAG");
        return 0;
    }

    static int custom_rmdir(const char *file_path) {
        char full_path[9999];
        int result;

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        if ((result = rmdir(full_path)) == -1) return -errno;

        custom_logger(full_path, "RMDIR", "FLAG");
        return 0;
    }

    static int custom_flush(const char *file_path, struct fuse_file_info *file_info) {
        // Implementasi fungsi custom_flush
        return 0;
    }

    static int custom_release(const char *file_path, struct fuse_file_info *file_info) {
        // Implementasi fungsi custom_release
        return 0;
    }

    static int custom_getxattr(const char *file_path, const char *name, char *value, size_t size) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0) {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int result = lgetxattr(full_path, name, value, size);
        if (result == -1) return -errno;

        return result;
    }

    static int custom_setxattr(const char *file_path, const char *name, const char *value, size_t size, int flags) {
        char full_path[9999];

        if (strcmp(file_path, "/") == 0)
        {
            file_path = myPath;
            sprintf(full_path, "%s", file_path);
        } else sprintf(full_path, "%s%s", myPath, file_path);

        int result = lsetxattr(full_path, name, value, size, flags);
        if (result == -1) return -errno;
        return 0;
    }

    static int custom_truncate(const char *path, off_t size) {
        char full_path[9999];
        sprintf(full_path, "%s%s", myPath, path);

        int result = truncate(full_path, size);
        if (result == -1) return -errno;

        return 0;
    }

    static int custom_ftruncate(const char *path, off_t size, struct fuse_file_info *fi) {
        char full_path[9999];
        sprintf(full_path, "%s%s", myPath, path);

        int result = ftruncate(fi->fh, size);
        if (result == -1) return -errno;

        return 0;
    }

    static struct fuse_operations custom_operations = {
        .getattr = custom_getattr,
        .access = custom_access,
        .readdir = custom_readdir,
        .read = custom_read,
        .truncate = custom_truncate,
        .ftruncate = custom_ftruncate,
        .open = custom_open,
        .write = custom_write,
        .mkdir = custom_mkdir,
        .create = custom_create,
        .unlink = custom_unlink,
        .rmdir = custom_rmdir,
        .rename = custom_rename,
        .flush = custom_flush,
        .release = custom_release,
        .setxattr = custom_setxattr,
        .getxattr = custom_getxattr,
    };

    int main(int argc, char *argv[]) {
        umask(0);
        return fuse_main(argc, argv, &custom_operations, NULL);
    }

```

## cara compile :

gcc -Wall `pkg-config fuse --cflags` -D_FILE_OFFSET_BITS=64 easy.c -o easy `pkg-config fuse --libs`

## cara run :
./easy /tmp/ini_fuse

## cara matikan :
fusermount -u /tmp/fuse

## untuk cek bisa pakai opsi dibawah ini :
mkdir, rmdir, copy file ke filesystem, sama remove file di filesystem

<a href="https://ibb.co/84YkCRv"><img src="https://i.ibb.co/k6mZ7rb/Screenshot-2023-11-18-134713.png" alt="Screenshot-2023-11-18-134713" border="0"></a><br /><a target='_blank' href='https://id.imgbb.com/'></a><br />

## Solution Soal 3D

## Kendala Soal 3
- ERROR SAAT COMPILE
- ERROR SAAT RUN
- KURANG MODULAR_
- TIDAK MENEMUKAN CARA UNTUK MEMECAH FILE
