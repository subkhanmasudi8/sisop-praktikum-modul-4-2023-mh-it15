#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

#define PORT 8080
#define MAX_BUFFER_SIZE 1024
#define WEBTOON_FILE_PATH "webtoon.csv"

void send_title_with_number(int new_socket, int number, const char* title) {
    char response[MAX_BUFFER_SIZE];
    sprintf(response, "%d. %s", number, title);
    send(new_socket, response, strlen(response), 0);
}

void handle_client_request(int new_socket) {
    char buffer[MAX_BUFFER_SIZE] = {0};
    FILE *webtoon_file;
    char line[MAX_BUFFER_SIZE];

    webtoon_file = fopen(WEBTOON_FILE_PATH, "r+");
    if (webtoon_file == NULL) {
        perror("Error opening webtoon.csv");
        exit(EXIT_FAILURE);
    }

    while (1) {
        read(new_socket, buffer, sizeof(buffer));

        printf("Received: %s\n", buffer);

        int titles_found = 0; 

        if (strncmp(buffer, "list all", 8) == 0) {
            int number = 1;
            fseek(webtoon_file, 0, SEEK_SET);  
            while (fgets(line, sizeof(line), webtoon_file) != NULL) {
                send_title_with_number(new_socket, number, line);
                number++;
                titles_found = 1;
            }
            if (!titles_found) {
                send(new_socket, "Maaf, tidak ada judul webtoons yang sesuai.", sizeof("Maaf, tidak ada judul webtoons yang sesuai."), 0);
            } else {
                send(new_socket, "Listed all webtoons.", sizeof("Listed all webtoons."), 0);
            }
        } else if (strncmp(buffer, "genre", 5) == 0) {
            char genre[20];
            sscanf(buffer, "genre %s", genre);

            int number = 1;
            fseek(webtoon_file, 0, SEEK_SET);  
            while (fgets(line, sizeof(line), webtoon_file) != NULL) {
                if (strstr(line, genre) != NULL) {
                    send_title_with_number(new_socket, number, line);
                    number++;
                    titles_found = 1;
                }
            }
        } else if (strncmp(buffer, "hari", 4) == 0) {
            char day[20];
            sscanf(buffer, "hari %s", day);

            int number = 1;
            fseek(webtoon_file, 0, SEEK_SET); 
            while (fgets(line, sizeof(line), webtoon_file) != NULL) {
                if (strstr(line, day) != NULL) {
                    send_title_with_number(new_socket, number, line);
                    number++;
                    titles_found = 1;
                }
            }
        } else if (strncmp(buffer, "add", 3) == 0) {
            char title[50], genre[20], day[20];
            sscanf(buffer, "add %s %s %s", day, genre, title);

            fprintf(webtoon_file, "%s,%s,%s\n", day, genre, title);
            fflush(webtoon_file);

            send(new_socket, "Entry added successfully.", sizeof("Entry added successfully."), 0);
        } else if (strncmp(buffer, "delete", 6) == 0) {
            char title[50];
            sscanf(buffer, "delete %s", title);

            FILE *temp_file = fopen("webtoon_temp.csv", "w");
            if (temp_file == NULL) {
                perror("Error creating temporary file");
                exit(EXIT_FAILURE);
            }

            fseek(webtoon_file, 0, SEEK_SET);
            while (fgets(line, sizeof(line), webtoon_file) != NULL) {
                if (strstr(line, title) == NULL) {
                    fprintf(temp_file, "%s", line);
                }
            }

            fclose(webtoon_file);
            fclose(temp_file);

            remove(WEBTOON_FILE_PATH);
            rename("webtoon_temp.csv", WEBTOON_FILE_PATH);

            send(new_socket, "Entry deleted successfully.", sizeof("Entry deleted successfully."), 0);

            webtoon_file = fopen(WEBTOON_FILE_PATH, "r+");
            if (webtoon_file == NULL) {
                perror("Error opening updated webtoon.csv");
                exit(EXIT_FAILURE);
            }
        } else if (strncmp(buffer, "exit", 4) == 0) {
            break;
        } else {
            send(new_socket, "Invalid Command", sizeof("Invalid Command"), 0);
        }

        if (!titles_found) {
            send(new_socket, "Maaf, tidak ada judul webtoons yang sesuai.", sizeof("Maaf, tidak ada judul webtoons yang sesuai."), 0);
        }

        memset(buffer, 0, sizeof(buffer));
    }

    fclose(webtoon_file);
    close(new_socket);
}

int main() {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    while (1) {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        if (fork() == 0) {
            close(server_fd);
            handle_client_request(new_socket);
            exit(0);
        } else {
            close(new_socket);
        }
    }

    return 0;
}

