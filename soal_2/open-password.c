#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char byte;

byte get_number_b64_from_symbol_0_64(byte b) {
    if (b >= 0x41 && b <= 0x5a)
        return b - 0x41;
    if (b >= 0x61 && b <= 0x7a)
        return b - 0x47;
    if (b >= 0x30 && b <= 0x39)
        return b + 0x4;
    if (b == 0x2b)
        return 0x3e;
    if (b == 0x2f)
        return 0x3f;
    if (b == 0x3d)
        return 0xff;
    return 0xff;
}

byte *from_b64_bytes(char b64[]) {
    if (strlen(b64) % 4 != 0)
        return NULL;

    byte *ret = (byte *)malloc(1);
    ret[0] = '\0';

    for (int i = 0; i < strlen(b64); i += 4) {
        byte b_ = get_number_b64_from_symbol_0_64(b64[i]) << 2;
        b_ += get_number_b64_from_symbol_0_64(b64[i + 1]) >> 4;
        ret = (byte *)realloc(ret, strlen(ret) + 2); 
        sprintf((char *)ret, "%s%c", (char *)ret, b_);

        if (get_number_b64_from_symbol_0_64(b64[i + 2]) == 0xff)
            break;

        b_ = get_number_b64_from_symbol_0_64(b64[i + 1]) << 4;
        b_ += get_number_b64_from_symbol_0_64(b64[i + 2]) >> 2;
        ret = (byte *)realloc(ret, strlen(ret) + 2);
        sprintf((char *)ret, "%s%c", (char *)ret, b_);

        if (get_number_b64_from_symbol_0_64(b64[i + 3]) == 0xff)
            break;

        b_ = get_number_b64_from_symbol_0_64(b64[i + 2]) << 6;
        b_ += get_number_b64_from_symbol_0_64(b64[i + 3]);
        ret = (byte *)realloc(ret, strlen(ret) + 2);
        sprintf((char *)ret, "%s%c", (char *)ret, b_);
    }
    return ret;
}

int main() {
    FILE *file = fopen("zip-pass.txt", "r");
    if (file == NULL) {
        perror("Error opening file");
        return 1;
    }

    char encodedPassword[0xff];
    if (fgets(encodedPassword, sizeof(encodedPassword), file) == NULL) {
        perror("Error reading password from file");
        fclose(file);
        return 1;
    }

    encodedPassword[strcspn(encodedPassword, "\n")] = 0;

    fclose(file);

    byte *decodedText = from_b64_bytes(encodedPassword);

    if (decodedText != NULL) {
        printf("Decoded text: \"%s\"\n", decodedText);
        free(decodedText);
    } else {
        printf("Invalid Base64 input\n");
    }

    return 0;
}

